/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kryptonita;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.io.Console;
import java.io.FileInputStream;
import java.security.MessageDigest;

/**
 *
 * @author davi.menezes
 */
public class Main {
    
    private static final boolean DEBUG = true;
    private static boolean indiceEncontrado = false;
    private static final int VARIACAO_BYTE = -8;
    private static final int HEADER = 50;
    private static final int FOOTER = 50;
    private static final int COVA = 10;
    private static final int BYTE_KEY = 60;
    private static int[] bytesSkip = {};
    private static int[] bytesKey = {};
    private static String password;
    private static String passwordTyped;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
        try {
            
            if(args.length < 1){
                Main.showHelp();
            }
            
            switch(args[0]){
                case "help":
                     Main.showHelp();
                break;
                case "--help":
                     Main.showHelp();
                break;
                case "-help":
                     Main.showHelp();
                break;
                case "version":
                     Main.showVersion();
                     Main.showSplash();
                     System.exit(0);
                break;
                case "--version":
                     Main.showVersion();
                     Main.showSplash();
                     System.exit(0);
                break;
                case "-version":
                     Main.showVersion();
                     Main.showSplash();
                     System.exit(0);
                break;
                   
                default:
            }
            
            if(args.length < 2){
                throw new Exception("Number of args invalid. arg 0 (-c or crypt) or (-d or decrypt). arg 1 filename.");
            }
            
            if(!args[0].equals("crypt")
            && !args[0].equals("decrypt")
            && !args[0].equals("-c")
            && !args[0].equals("-d")
            ){
                throw new Exception("arg 0:"+args[0]+" invalid. Use -c or -d");
            }
            
            if(args[0].equals("-d")){
                args[0] = "decrypt";
            }
            
            if(args[0].equals("-c")){
                args[0] = "crypt";
            }
            
            File f = new File(args[1]);
            if(!f.exists()){
                throw new Exception("File not found: "+args[1]);
            }
            
            if (args[1].indexOf(".jar") > -1)  {
                throw new Exception("You can't Encrypt/Decrypt jar files.");
            }
            
            Path path = Paths.get(args[1]);
            byte[] data = Files.readAllBytes(path);
            
            //
            
            if(args.length < 2){
                throw new Exception("Number of params invalid. use: -c file_name -p or -c file_name -p password");
            }
            
            //informa a senha em texo plano
            if(args.length == 4 && args[2].equals("-p")){
                if(args[0].equals("decrypt")){
                    Main.passwordTyped = args[3];
                }
                
                Main.processFile(args[1], args[0],Main.sha256(args[3]));
            }
            
            //informa a senha depois
            if(args.length == 3 && args[2].equals("-p")){
                if(args[0].equals("decrypt")){
                    throw new Exception("type password after '-p'.");
                }
                String senha = Main.askPassword();
                Main.processFile(args[1], args[0],Main.sha256(senha));
            }
            
            //sem senha
            if(args.length == 2){
                Main.processFile(args[1], args[0],null);
            }
            
            Main.showSplash();
            
            
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            //ex.printStackTrace();
        }
        
    }
    
    private static String sha256(String str) throws Exception{
    
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(str.getBytes());

        byte byteData[] = md.digest();

        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }

       
        //convert the byte to hex format method 2
        StringBuffer hexString = new StringBuffer();
    	for (int i=0;i<byteData.length;i++) {
    		String hex=Integer.toHexString(0xff & byteData[i]);
   	     	if(hex.length()==1) hexString.append('0');
   	     	hexString.append(hex);
    	}
        
    	return hexString.toString();
    }
    
    private static void showHelp(){
        
        Main.showSplash();
        
        System.out.println(
                             "-c    - Encrypt a file \n"+
                             "-d    - Decrypt a file \n"+
                             "-p    - (optional) password \n"+
                             "Example: \n"+
                             "java -jar Cryptonika.jar -c file_name.zip \n"
                     );
                     System.exit(0);
    }
    
    private static void showSplash(){
    
        StringBuilder sb = new StringBuilder();
            
        sb.append("╔═════════════ **** ══════════════════════╗\n" +
                  "        °-° KRIPTONITA °-°              \n" +
                  "    Desenvolvido por Davi Menezes        \n" +
                  "    <davifelipems@hotmail.com>     \n" +
                  "╚═════════════ **** ══════════════════════╝");

        System.out.println(sb.toString());
    }
    
    private static void showVersion(){
    
        StringBuilder sb = new StringBuilder();
            
        sb.append("Version 1.4.2\n");
        
        System.out.println(sb.toString());
    }
    
        private static String getExtension(String filename){

        StringBuilder sb = new StringBuilder();
        for (int i = filename.length() -1; i >= 0; i--) {
            char c = filename.charAt(i);
            if(c == '.'){
                break;
            }
            sb.append(c);
        }

        String extInvert = sb.toString();
        sb = new StringBuilder();
        for (int i = extInvert.length() -1; i >= 0; i--) {
            sb.append(extInvert.charAt(i));
        }

        return sb.toString();
    }

    private static String askPassword() throws Exception{

            Console console = System.console();
            if (console == null) {
                System.out.println("Couldn't get Console instance");
                System.exit(0);
            }

            char passwordArray[] = console.readPassword("Type your password: ");
            String saida = new String(passwordArray);
            char passwordArray2[] = console.readPassword("Retype your password: ");
            String saida2 = new String(passwordArray2);
            
            if(!saida.equals(saida2)){
                  throw new Exception("passwords not match.");
            }
            

            return saida;
    }
    
    private static boolean checkPassword(String password) throws Exception{

            Console console = System.console();
            if (console == null) {
                System.out.println("Couldn't get Console instance");
                System.exit(0);
            }
            
            String saida = "";
            if(Main.passwordTyped == null 
            || Main.passwordTyped.isEmpty()){
                char passwordArray[] = console.readPassword("Type your password: ");
                saida = Main.sha256(new String(passwordArray));
            }else{
                saida = Main.sha256(Main.passwordTyped);
            }
            
            if(!saida.equals(password)){
                  return false;
            }
            
            return true;
    }

    private static void processFile(String fileName,String action,String password)throws Exception{

            Path path = Paths.get(fileName);
            String ext = Main.getExtension(fileName);
            byte[] data = Files.readAllBytes(path);

            if(action.equals("decrypt")){
                Main.indiceEncontrado = false;
                Main.bytesSkip = new int[]{};
                Main.bytesKey = new int[]{};

                if(data.length < 5){
                    throw new Exception("o arquivo "+fileName+" não foi criptografado pelo kriptonita");
                }

                data = Main.getDados("k_-_a",
                                     data,
                                     "Main.bytesSkip",
                                     '!');
                
                data = Main.getDados("+-_-)",
                                     data,
                                     "Main.bytesKey",
                                     '.');

                data = Main.getDados("p_w;d",
                                     data,
                                     "Main.password",
                                     '*');
               
                if(!Main.password.isEmpty()
                && !Main.checkPassword(Main.password)){
                  throw new Exception("Invalid password.");
                }
                
                
                if(!Main.indiceEncontrado){
                    throw new Exception("o arquivo "+fileName+" não foi criptografado pelo kriptonita");
                }
                
                for (int i = 0; i < Main.bytesKey.length; i++) {
                     data[Main.bytesKey[i]] = Byte.decode(Main.BYTE_KEY+"");
                }
            }


            if(data.length <= 5000){
                for (int i = 0; i < data.length; i++) {
                   
                    data[i] = Main.processByte(data[i],i, action);
                }
            }else{
                int salto = data.length/4;
                
                int header = Main.HEADER;
                int footer = Main.FOOTER;
                int cova = Main.COVA;

                for (int i = 0; i < header; i++) {
                    data[i] = Main.processByte(data[i],i, action);
                }

                if(header > salto){
                    header = salto;
                }
                if(cova > salto){
                    cova = salto;
                }

                for (int i = header+1; i < data.length; i+=salto) {

                    if(i >= (data.length - footer)){
                        break;
                    }

                    data[i] = Main.processByte(data[i],i, action);

                    if(cova < data.length){
                        for (int j = i+1; j < cova; j++) {
                            data[j] = Main.processByte(data[j],j, action);
                        }
                        cova+=salto;
                    }
                }

                int inicioFooter = data.length - footer;

                
                for (int i = inicioFooter; i < data.length; i++) {
                    data[i] = Main.processByte(data[i],i, action);
                }

            }


            if(action.equals("crypt")){

                data = Main.addDados("k_-_a",
                                    Main.bytesSkip,
                                    '|',
                                    data,
                                    '!');

                    if(password == null){
                        password = "";
                    }

                    data = Main.addDados("p_w;d",
                                    password,
                                    '|',
                                    data,
                                    '*');

                int kp = 0;
                for (int i = data.length -1; i >0 ; i--) {
                    if(data[i] == Main.BYTE_KEY 
                    && kp < 100
                    ){
                        data[i] = Byte.decode("123");
                        Main.bytesKey= Main.addElementInt(i, Main.bytesKey);
                        kp++;
                    }
                }
                
                data = Main.addDados("+-_-)",
                                        Main.bytesKey,
                                        '|',
                                        data,
                                        '.');
            }
            
            String extAction = (action.equals("crypt") ? ".crt." : ".dct.");

            String outFile = fileName.replaceAll("."+ext, "")+extAction+ext;

            FileOutputStream fos = new FileOutputStream(outFile);
            fos.write(data);
            fos.close();

            if(Main.DEBUG) System.out.println("reading "+fileName+" checksum sha-256 > "+Main.checksumSha256(fileName));
            if(Main.DEBUG) System.out.println("file    "+outFile+ " created. checksum sha-256 > "+Main.checksumSha256(outFile));

    }

    private static byte[] getDados(String key,byte[] data, String evalDados,char end) throws Exception{

        
        int[] chave = new int[key.length()];
        StringBuilder saida = new StringBuilder();
        int l=0;
        for (int i = key.length() -1; i >= 0; i--) {
            char c = key.charAt(i);
            chave[l] = (int)c;l++;
        }

        int[] removerData = null;
        int diferenca = 0;
        for (int i = data.length; i >0 ; i--) {


            diferenca++;
            boolean chaveEncontrada = true;
            for (int y = 0; y < chave.length; y++) {
                int ref = y+1;
                byte dataAtual = data[i-ref];
                int chaveAtual =chave[y];

                if(dataAtual != chaveAtual){
                    chaveEncontrada = false;
                    break;
                }
            }

            if(chaveEncontrada){
                Main.indiceEncontrado = true;
                removerData = new int[(data.length - i) +chave.length];
               
                for (int y = 0; y < chave.length; y++) {
                    int ref = y+1;
                    removerData[y] = i-ref;
                }
                int iRemvData = chave.length;

                for (int j = i; j < data.length; j++) {
                    byte byteChar = data[j];

                    if(byteChar == (int)end){
                        removerData[iRemvData] = j;
                        iRemvData++;
                        break;
                    }

                    char c = (char)byteChar;
                    StringBuilder sb = new StringBuilder();


                    while (byteChar != 124 && j <= data.length) {
                        removerData[iRemvData] = j;
                        iRemvData++;

                        sb.append(c);
                        j++;
                        byteChar = data[j];
                        c = (char)byteChar;
                    }

                    if(byteChar == 124){
                        removerData[iRemvData] = j;
                        iRemvData++;
                    }

                    Integer indice = Integer.parseInt(sb.toString());

                    if(evalDados.equals("Main.bytesSkip")){
                        Main.bytesSkip = Main.addElementInt(indice, Main.bytesSkip);
                    }
                    if(evalDados.equals("Main.bytesKey")){
                        Main.bytesKey = Main.addElementInt(indice, Main.bytesKey);
                    }
                    if(evalDados.equals("Main.password")){
                        int chaByte = Integer.parseInt(sb.toString());
                        saida.append((char)chaByte);
                    }


                }
                break;
            }
        }

        if(evalDados.equals("Main.password")){
            Main.password = saida.toString();
        }
        
        if(removerData != null){
            int qtdZero = 0;
            for (int i = 0; i < removerData.length; i++) {
                if(removerData[i] == 0){
                    qtdZero++;
                }
            }

            int[] newRemoverData = new int[removerData.length - qtdZero];

            int kR = 0;
            for (int i = 0; i < removerData.length; i++) {
                if(removerData[i] != 0){
                    newRemoverData[kR] = removerData[i];kR++;
                }

            }

            removerData = newRemoverData;

            data = Main.removeElement(removerData, data);
        }


        return data;
    }

    private static byte[] addDados(String key,String valor,char separador,byte[] data,char end) throws Exception{

        int[] chave = new int[key.length()];

        for (int i = 0; i < key.length(); i++) {
            char c = key.charAt(i);
            if(separador == c){
                throw new Exception("key "+key+" nao pode conter caracteres igual ao separador "+separador);
            }
            chave[i] = (int)c;
        }

        for (int i = 0; i < chave.length; i++) {
            data = Main.addElement(chave[i], data);
        }

        int[] valores = new int[valor.length()];

        for (int i = 0; i < valor.length(); i++) {
            char c = valor.charAt(i);
            valores[i] = (int)c;
        }

        byte[] byteSkyp = new byte[]{};
        for (int i = 0; i < valores.length; i++) {
            byteSkyp = new byte[]{};
            byteSkyp = Main.toBytes(valores[i]);
            byteSkyp = Main.addElement((int)separador, byteSkyp);
            data = Main.addElement(byteSkyp, data);
        }
        data = Main.addElement((int)end, data);

        return data;
    }
    private static byte[] addDados(String key,int[] valores,char separador,byte[] data,char end) throws Exception{

        int[] chave = new int[key.length()];

        for (int i = 0; i < key.length(); i++) {
            char c = key.charAt(i);
            if(separador == c){
                throw new Exception("key "+key+" nao pode conter caracteres igual ao separador "+separador);
            }
            chave[i] = (int)c;
        }

        for (int i = 0; i < chave.length; i++) {
            data = Main.addElement(chave[i], data);
        }
        
        byte[] byteSkyp = new byte[]{};
        for (int i = 0; i < valores.length; i++) {
            byteSkyp = new byte[]{};
            byteSkyp = Main.toBytes(valores[i]);
            byteSkyp = Main.addElement((int)separador, byteSkyp);
            data = Main.addElement(byteSkyp, data);
        }
        data = Main.addElement((int)end, data);

        return data;
    }

    private static byte[] toBytes(int i){
        
        Integer numero = i;
        String numeroStr = numero.toString();
        
        byte[] saida = new byte[numeroStr.length()];
        
        for(int j = 0; j< numeroStr.length(); j++){
            char c = numeroStr.charAt(j);
            Integer decimal = (int)c;
            saida[j] = Byte.decode(decimal.toString());
        }

      return saida;
    }

    private static byte[] removeElement(int[] byteInt,byte[] arrayByte){

        for (int i = 0; i < byteInt.length; i++) {
            if( byteInt[i] < 0 && byteInt[i] < arrayByte.length){
                return arrayByte;
            }
        }

        byte[] saida = new byte[arrayByte.length - byteInt.length];
        int k = 0;
        boolean addSaida = true;
        for (int i = 0; i < arrayByte.length; i++) {
            addSaida = true;
            for (int j = 0; j < byteInt.length; j++) {
                if(i == byteInt[j]){
                    addSaida = false;
                }
            }

            if(addSaida){
                saida[k] = arrayByte[i];k++;
            }
        }
        
        return saida;
    }
    

    private static int[] addElementInt(int intNum,int[] arrayInt){

        int[] destination = new int[arrayInt.length + 1];
        for (int i = 0; i < arrayInt.length; i++) {
            destination[i] = arrayInt[i];
        }
        destination[arrayInt.length] = intNum;
        arrayInt = destination;

        return arrayInt;
    }

    private static byte[] addElement(byte[] byteIn,byte[] arrayByte){

        int destinationLength = arrayByte.length + byteIn.length;
        byte[] destination = new byte[destinationLength];
        for (int i = 0; i < arrayByte.length; i++) {
            destination[i] = arrayByte[i];
        }
        int k =0;
        for (int i = arrayByte.length; i < destinationLength; i++) {
            destination[i] = byteIn[k];k++;

        }
       
        arrayByte = destination;

        return arrayByte;
    }
    private static byte[] addElement(int byteInt,byte[] arrayByte){
        
        byte baite = Byte.decode(Integer.toString(byteInt));

        byte[] destination = new byte[arrayByte.length + 1];
        for (int i = 0; i < arrayByte.length; i++) {
            destination[i] = arrayByte[i];
        }
        destination[arrayByte.length] = baite;
        arrayByte = destination;

        return arrayByte;
    }
    
    private static String checksumSha256(String fileName) throws Exception{
        
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        FileInputStream fis = new FileInputStream(fileName);

        byte[] dataBytes = new byte[1024];

        int nread = 0;
        while ((nread = fis.read(dataBytes)) != -1) {
          md.update(dataBytes, 0, nread);
        };
        byte[] mdbytes = md.digest();

        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < mdbytes.length; i++) {
          sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
        }

       //convert the byte to hex format method 2
        StringBuffer hexString = new StringBuffer();
    	for (int i=0;i<mdbytes.length;i++) {
    	  hexString.append(Integer.toHexString(0xFF & mdbytes[i]));
    	}

    	return hexString.toString();
    }
   
    private static byte processByte(byte baite,int i,String action){

        String byteStr = Byte.toString(baite);

        Integer byteInteger = Integer.parseInt(byteStr);

        Integer byteIntegerAlterado = 0;

        if(action.equals("crypt")){
             byteIntegerAlterado = byteInteger+Main.VARIACAO_BYTE;
        }else{
             byteIntegerAlterado = byteInteger-Main.VARIACAO_BYTE;
        }
        
        if((byteIntegerAlterado >=128 || byteIntegerAlterado <=-128)
        && action.equals("crypt")){
                 Main.bytesSkip = Main.addElementInt(i, Main.bytesSkip);
                 byteIntegerAlterado = byteInteger;
        }


        if(action.equals("decrypt")){
            for (int j = 0; j < Main.bytesSkip.length; j++) {
                if(bytesSkip[j] == i){
                    byteIntegerAlterado = byteInteger;
                }
            }
            //System.out.println("compare > "+i+" > "+byteIntegerAlterado);
        }
        
        return Byte.decode(byteIntegerAlterado.toString());
        
    }
    
}
